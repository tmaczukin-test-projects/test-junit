export CGO_ENABLED ?= 0
export GOPATH ?= ".go/"

goJunitReport ?= $(GOPATH)/bin/go-junit-report

testsDir = ./.tests

.PHONY: tests
tests: $(testsDir) $(goJunitReport)
	go test ./... -cover -covermode=count -coverprofile=$(testsDir)/coverage.out -v | tee $(testsDir)/output.txt
	# Generating jUnit report: $(testsDir)/junit.xml
	@$(goJunitReport) < $(testsDir)/output.txt > $(testsDir)/junit.xml

.PHONY: tests-race
tests-race: $(testsDir) $(goJunitReport)
	go test ./... -race -v | tee $(testsDir)/output.txt
	# Generating jUnit report: $(testsDir)/junit-race.xml
	@$(goJunitReport) < $(testsDir)/output.txt > $(testsDir)/junit-race.xml

$(testsDir):
	# Preparing tests output directory
	@mkdir -p $@

$(goJunitReport):
	# Installing go-junit-report
	@go install github.com/jstemmer/go-junit-report

